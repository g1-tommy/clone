def say_hello():
	animals = ['cat', 'dog', 'hamster']
	foods = [
	    'ramen',
	    'ramyun',
	    'spaghetti',
	    'udong',
	]
	colors = [
	    'red',
	    'blue',
	    'green',
	    'yellow'
	]
	print('Hello, world!')

if __name__ == "__main__":
	say_hello()
